﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace Exercici_1
{
    class Program
    {
        static void Main()
        {
            CountLinesThread();
        }

        static void CountLinesThread()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Aquesta linia està en el progrés principal");

            /***** Pas de paràmetre en un thread***/
            Thread FirstThread = new Thread(CountLinesFunc);
            FirstThread.Start(@"../../../../LoremIpsum.txt");
            
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Thread SecondThread = new Thread(CountLinesFunc);
            SecondThread.Start(@"../../../../FitxerLLarg.txt");
            
            Console.ForegroundColor = ConsoleColor.Yellow;
            Thread ThirdThread = new Thread(CountLinesFunc);
            ThirdThread.Start(@"../../../../FitxerCurt.txt");
        }

        static void CountLinesFunc(object file)
        {
            string fileName = Convert.ToString(file);
            var fileLines = File.ReadLines(fileName).Count();
            Console.WriteLine("Quantitat de lineas del fitxer: " + fileLines);
        }

    }
}
